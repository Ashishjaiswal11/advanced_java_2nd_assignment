package com.nagarro.hibernate.utility;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class CallingFactory {

	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();

	// for getting Session factory obj for conection
	public SessionFactory factoryCall() {
		return factory;
	}

}

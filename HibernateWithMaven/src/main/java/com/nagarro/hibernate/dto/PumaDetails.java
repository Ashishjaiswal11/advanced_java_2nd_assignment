package com.nagarro.hibernate.dto;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class PumaDetails {

	@Id
	private String id;
	private String name;
	private String colour;
	private String gender;
	private String size;
	private String price;
	private String rating;
	private String availability;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getAvailability() {
		return availability;
	}

	public void setAvailability(String availability) {
		this.availability = availability;
	}

	public PumaDetails(String id, String name, String colour, String gender, String size, String price, String rating,
			String availability) {
		super();
		this.id = id;
		this.name = name;
		this.colour = colour;
		this.gender = gender;
		this.size = size;
		this.price = price;
		this.rating = rating;
		this.availability = availability;
	}

	@Override
	public String toString() {
		return " " + id + "    | " + name + " |  " + colour + " |   " + gender + "    |   " + size + "   | " + price
				+ "|  " + rating + "  |     " + availability + "   |";
	}

	public PumaDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

}

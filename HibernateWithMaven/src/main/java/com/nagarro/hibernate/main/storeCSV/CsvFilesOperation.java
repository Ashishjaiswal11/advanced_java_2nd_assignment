package com.nagarro.hibernate.main.storeCSV;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.nagarro.hibernate.dto.AdidasDetails;
import com.nagarro.hibernate.dto.NikeDetails;
import com.nagarro.hibernate.dto.PumaDetails;
import com.nagarro.hibernate.utility.CallingFactory;

public class CsvFilesOperation {

	CallingFactory sc = new CallingFactory();
	SessionFactory factory = sc.factoryCall();

	public void storeCsvDataBase(String filename) throws FileNotFoundException {

		// arr arraylist obj create for storing file token obj(string)
		ArrayList<String> arr;

		// for get the token form the file and store into the database

		Scanner sc = new Scanner(new File(filename));
		while (sc.hasNext()) {

			Session session = factory.openSession();
			Transaction tx = session.beginTransaction();
			String line = sc.nextLine().toUpperCase().toString();
			if (!line.isEmpty()) {
				// for spilit the string (line string) using delimeter |
				StringTokenizer token = new StringTokenizer(line, "|");
				arr = new ArrayList<String>(line.length());
				// for store the string elements into the list object
				while (token.hasMoreTokens()) {
					arr.add(token.nextToken());
				}

				try {
					if (filename.equalsIgnoreCase("C:\\Users\\ashishjaiswal\\Desktop\\CSVFiles\\Adidas.csv")
							&& (arr.get(0).equalsIgnoreCase("id") != true)) {
						AdidasDetails ad = new AdidasDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
								arr.get(5), arr.get(6), arr.get(7));

						session.save(ad);
						session.getTransaction().commit();
						session.close();
					}

					else if (filename.equalsIgnoreCase("C:\\Users\\ashishjaiswal\\Desktop\\CSVFiles\\Nike.csv")
							&& (arr.get(0).equalsIgnoreCase("id") != true)) {
						NikeDetails nd = new NikeDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
								arr.get(5), arr.get(6), arr.get(7));
						session.save(nd);
						session.getTransaction().commit();
						session.close();

					}

					else if (filename.equalsIgnoreCase("C:\\Users\\ashishjaiswal\\Desktop\\CSVFiles\\Puma.csv")
							&& (arr.get(0).equalsIgnoreCase("id") != true)) {
						PumaDetails pd = new PumaDetails(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
								arr.get(5), arr.get(6), arr.get(7));
						session.save(pd);
						try {
							session.getTransaction().commit();
						} catch (Exception e) {
							e.printStackTrace();
						}
						session.close();

					}
				} catch (Exception e) {
					e.printStackTrace();

				}
			}
		}
	}
}

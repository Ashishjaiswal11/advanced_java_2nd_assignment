package com.nagarro.hibernate.main.storeCSV;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import com.nagarro.hibernate.dto.NikeDetails;
import com.nagarro.hibernate.search.outputprefence.OutputPrefrence;
import com.nagarro.hibernate.search.outputprefence.SearchDetails;

//user define exception
class MyException extends Exception {
}

public class MainUserInput {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
//       take required input from the user
		boolean yes = true;
		while (yes) {
			System.out.println("Enter T-Shirt Colour");
			String colour = sc.nextLine().toUpperCase();

			boolean a = true;
			String size = "";
			while (a) {
				System.out.println("Enter T-Shirt Size");
				size = sc.nextLine().toUpperCase();
				if (size.equals("S") || size.equals("M") || size.equals("L") || size.equals("XL")
						|| size.equals("XXL")) {
					a = false;
				} else {
					try {
						throw new MyException();
					} catch (MyException ex) {
						System.out.println("Wrong input please select  S, M , L , XL, XXL");
					}
				}
			}
			boolean b = true;
			String gender = "";
			while (b) {
				System.out.println("Enter  Gender");
				System.out.println("Select M or F or U");
				gender = sc.nextLine().toUpperCase();
				if (gender.equals("M") || gender.equals("F") || gender.equals("U")) {
					b = false;
				} else {
					try {
						throw new MyException();
					} catch (MyException ex) {
						System.out.println("Wrong input please select M for male , F for female, U for uni...");
					}
				}
			}
			System.out.println("Please->  Select Output Prefrence");
			System.out.println("press 1:  sorted only by Price ");
			System.out.println("press 2:  sorted only by Rating");
			System.out.println("press 3:  sorted by both Price and Rating");
			int choice = sc.nextInt();

			CsvFilesOperation cf = new CsvFilesOperation();
			File folder = new File("C:\\Users\\ashishjaiswal\\Desktop\\CSVFiles");
			for (File file : folder.listFiles()) {
				if (!file.isDirectory()) {
					try {
						cf.storeCsvDataBase("C:\\Users\\ashishjaiswal\\Desktop\\CSVFiles\\" + file.getName());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			// for searching user input record into the filesb
			SearchDetails sd = new SearchDetails();

			sd.searchAdidas(colour, size, gender);

			sd.searchNike(colour, size, gender);

			sd.searchPuma(colour, size, gender);

			System.out.println("mission succesfull");

			SearchDetails sc1 = new SearchDetails();
//			// tshirtlist arraylist obj create for storing matching data

			System.out.println(
					"        Id        |        NAME         | COLOUR | GENDER | SIZE |  PRICE |  RATING  |AVAILABILITY|");
			ArrayList<Object> tShirtList = new ArrayList();
			try {
				tShirtList = sc1.record();
				// for getting result according to selected user output prefrence
				OutputPrefrence ot = new OutputPrefrence();
				// ot.outputPrefrence(choice, tShirtList);
				NikeDetails nd = new NikeDetails();
				for (Object data : tShirtList) {
					System.out.println(data);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			sc1.st = new ArrayList();

//		
			System.out.println("you want to search more records press yes or no");
			sc.nextLine();
			String n = sc.nextLine();
			if (n.equalsIgnoreCase("yes"))
				yes = true;
			else
				yes = false;

		}
	}
}

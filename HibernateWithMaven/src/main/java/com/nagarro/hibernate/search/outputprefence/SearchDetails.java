package com.nagarro.hibernate.search.outputprefence;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.nagarro.hibernate.dto.AdidasDetails;
import com.nagarro.hibernate.dto.NikeDetails;
import com.nagarro.hibernate.dto.PumaDetails;
import com.nagarro.hibernate.utility.CallingFactory;

public class SearchDetails {

	public static ArrayList<Object> st = new ArrayList();
	List<AdidasDetails> ad = null;
	List<NikeDetails> nd = null;
	List<PumaDetails> pd = null;

	CallingFactory cs = new CallingFactory();
	SessionFactory factory = cs.factoryCall();
	Session session = factory.openSession();
	Transaction tx = session.beginTransaction();

	public void searchAdidas(String colour, String size, String gender) {

		String query = "from AdidasDetails";
		Query q = session.createQuery(query);
		List<AdidasDetails> ad = q.list();
		for (AdidasDetails obj : ad) {
			try {
				if (obj.getColour().equalsIgnoreCase(colour) && obj.getSize().equalsIgnoreCase(size)
						&& obj.getGender().equalsIgnoreCase(gender)) {
					st.add(obj);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public void searchNike(String colour, String size, String gender) {
		String query = "from NikeDetails";
		Query q = session.createQuery(query);
		List<NikeDetails> nd = q.list();
		for (NikeDetails obj : nd) {
			try {
				if (obj.getColour().equalsIgnoreCase(colour) && obj.getSize().equalsIgnoreCase(size)
						&& obj.getGender().equalsIgnoreCase(gender)) {
					st.add(obj);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public void searchPuma(String colour, String size, String gender) {
		String query = "from PumaDetails";
		Query q = session.createQuery(query);
		List<PumaDetails> pd = q.list();
		for (PumaDetails obj : pd) {

			try {
				if (obj.getColour().equalsIgnoreCase(colour) && obj.getSize().equalsIgnoreCase(size)
						&& obj.getGender().equalsIgnoreCase(gender)) {
					st.add(obj);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public ArrayList<Object> record() {
		return st;
	}
}
